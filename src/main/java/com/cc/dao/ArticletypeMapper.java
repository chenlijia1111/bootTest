package com.cc.dao;

import com.cc.pojo.Articletype;

public interface ArticletypeMapper {
    int insert(Articletype record);

    int insertSelective(Articletype record);
}