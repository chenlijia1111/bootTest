package com.cc.dao;

import com.cc.pojo.Protocol;

public interface ProtocolMapper {
    int insert(Protocol record);

    int insertSelective(Protocol record);
    
    Integer updateProtocol(Protocol record);
    
    Protocol findByLang(String lang);
}