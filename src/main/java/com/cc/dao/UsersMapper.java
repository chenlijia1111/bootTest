package com.cc.dao;

import com.cc.pojo.Users;

public interface UsersMapper {
    int insert(Users record);

	int insertSelective(Users record);
	
	Users findUsers(Users users);
	
	Integer updatePSW(Users users);
	
	Integer addUser(Users users);

}