package com.cc.dao;

import com.cc.pojo.Article;

import java.util.List;
import java.util.Map;

public interface ArticleMapper {
    int insert(Article record);
    /**
     * 分类别分页
     * @param map
     * @return
     */
    List<Article> findArticleByType(Map<String, Object> map);
    /**
     * 查询当前类别总数
     * @param map
     * @return
     */
    Integer findAllcountByType(Map<String, Object> map);
    
    /**
     * 更新类别信息等
     * @param article
     * @return
     */
    Integer updateType(Article article);
    
    /**
     * 获取当前类别最大编号
     * @param type
     * @return
     */
    Integer findMaxNymberOfType(Integer type);
    /**
     * 更新种内编号
     * @param type
     * @return
     */
    Integer updateNumberOfType(Map<String, Integer> map);
    
    
    Article findArticleById(Integer id);
    
    Integer deleteArticleById(Integer id);
    /**
     * 查找上一篇文章
     * @param map
     * @return
     */
    Article findPreArticle(Map<String, Object> map);
    /**
     * 查找下一篇文章
     * @param map
     * @return
     */
    Article findNextArticle(Map<String, Object> map);
    /**
     * 推荐分页
     * @param map
     * @return
     */
    List<Article> listRecommend(Map<String, Object> map);
    /**
     * 查询推荐总数
     * @return
     */
    Integer findAllCountRecommend();
    /**
     * 更改文章是否推荐状态
     * @param id
     * @return
     */
    Integer updateIsRecommend(Map<String, Object> map);
    /**
     * 修改文章
     * @param article
     * @return
     */
    Integer updateArticle(Article article);
    
    List<Article> finAlldArticle();
}