package com.cc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cc.dao.UsersMapper;
import com.cc.pojo.Users;
import com.cc.service.UserService;
@Service
public class UserServiceimpl implements UserService{
	@Autowired
	private UsersMapper usersMapper;
	@Override
	//开启事务
	@Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,timeout=36000,rollbackFor=Exception.class)
	public void add() {
		Users users = new Users();
		users.setLoginname("aa");
		users.setLoginpwd("bb");
		usersMapper.insert(users);
	}

}
