package com.cc.pojo;

import java.util.Date;

public class Article {
    private Integer id;

    private String title;

    private Integer author;

    private Integer type;

    private String isrecommend;

    private Integer typenumber;

    private Date postmodified;

    private Date postdate;

    private String content;
    
    public Article() {
		// TODO Auto-generated constructor stub
	}

    public Article(Integer id, String title, String content) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAuthor() {
        return author;
    }

    public void setAuthor(Integer author) {
        this.author = author;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIsrecommend() {
        return isrecommend;
    }

    public void setIsrecommend(String isrecommend) {
        this.isrecommend = isrecommend;
    }

    public Integer getTypenumber() {
        return typenumber;
    }

    public void setTypenumber(Integer typenumber) {
        this.typenumber = typenumber;
    }

    public Date getPostmodified() {
        return postmodified;
    }

    public void setPostmodified(Date postmodified) {
        this.postmodified = postmodified;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", author=" + author + ", type=" + type + ", isrecommend="
				+ isrecommend + ", typenumber=" + typenumber + ", postmodified=" + postmodified + ", postdate="
				+ postdate + ", content=" + content + "]";
	}
    
    
}