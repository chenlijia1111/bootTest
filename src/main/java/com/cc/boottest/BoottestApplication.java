package com.cc.boottest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(value = "com.cc")
@EnableTransactionManagement
@MapperScan(value = "com.cc.dao")
public class BoottestApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(BoottestApplication.class, args);
	}
}
